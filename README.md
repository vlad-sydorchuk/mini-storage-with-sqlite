# Mini Storage #

This is simple project, where i implement mini storage with database and some commands.

### About project ###

+ Language: C#
+ Database: SQLite
+ IDE: Visual Studio 2017

### Commands ###

+ Add (Добавить)
+ Indexing (Индексация)
+ Delete (Удалить)
